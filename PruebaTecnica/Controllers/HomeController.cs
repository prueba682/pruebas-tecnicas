﻿using PruebaTecnica.DAL;
using PruebaTecnica.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PruebaTecnica.Controllers
{
    public class HomeController : Controller
    {
        private DataBaseContext db = new DataBaseContext();
        public ActionResult Index()
        {
            List<Facturas> lstFacturas = db.Facturas.ToList();
            return View(lstFacturas);
        }

        // GET: Home/Crear
        public ActionResult Crear()
        {
            int num_factura = db.Facturas.ToList().Count();
            List<Productos> lstProds = db.Productos.ToList();
            ViewBag.lstProds = lstProds;
            ViewBag.num_factura = num_factura + 1001;
            return View();
        }

        // GET: Home/Editar
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facturas factura = db.Facturas.Find(id);
            List<Detalles> detalles = db.Detalles.ToList().Where(a => a.IdFactura == id).ToList();
            List<Productos> lstProds = db.Productos.ToList();
            ViewBag.lstDetalles = detalles;
            ViewBag.lstProds = lstProds;
            if (factura == null)
            {
                return HttpNotFound();
            }
            return View(factura);
        }

        [HttpPost]
        public JsonResult RegistrarFactura(RequestData requestData)
        {
            try
            {
                //Almacena valor de la factura
                db.Facturas.Add(requestData.Factura);
                db.SaveChanges();
                //Almacena valor detalle
                foreach (Detalles item in requestData.LstDetalles)
                {
                    item.Factura = requestData.Factura;

                    db.Detalles.Add(item);
                }
                db.SaveChanges();
                return Json(requestData.Factura, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error");
            }
        }

        [HttpPost]
        public JsonResult EditarFactura(RequestData requestData)
        {
            try
            {
                db.Entry(requestData.Factura).State = EntityState.Modified;
                //Almacena valor detalle
                foreach (Detalles item in requestData.LstDetalles)
                {
                    db.Entry(item).State = EntityState.Modified;
                }
                db.SaveChanges();
                return Json(requestData.Factura, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error");
            }
        }

        [HttpPost]
        public JsonResult EliminarFactura(dynamic idFactura)
        {
            try
            {
                Facturas factura = db.Facturas.Find(idFactura);
                db.Facturas.Remove(factura);
                db.SaveChanges();
                return Json(factura, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error");
            }
        }

    }
}