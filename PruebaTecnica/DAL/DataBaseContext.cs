﻿using PruebaTecnica.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PruebaTecnica.DAL
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext() : base("PruebaTecnicaContext")
        {
        }

        public DbSet<Productos> Productos { get; set; }
        public DbSet<Facturas> Facturas { get; set; }
        public DbSet<Detalles> Detalles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}