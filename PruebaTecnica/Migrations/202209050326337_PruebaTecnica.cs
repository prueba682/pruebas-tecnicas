﻿namespace PruebaTecnica.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PruebaTecnica : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Detalles",
                c => new
                    {
                        IdDetalle = c.Int(nullable: false, identity: true),
                        IdFactura = c.Int(nullable: false),
                        IdProducto = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        PrecioUnitario = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdDetalle)
                .ForeignKey("dbo.Facturas", t => t.IdFactura, cascadeDelete: true)
                .ForeignKey("dbo.Productos", t => t.IdProducto, cascadeDelete: true)
                .Index(t => t.IdFactura)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        IdFactura = c.Int(nullable: false, identity: true),
                        NumeroFactura = c.Int(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        TipodePago = c.String(),
                        DocumentoCliente = c.String(),
                        NombreCliente = c.String(),
                        Subtotal = c.Double(nullable: false),
                        Descuento = c.Double(nullable: false),
                        IVA = c.Double(nullable: false),
                        TotalDescuento = c.Double(nullable: false),
                        TotalImpuesto = c.Double(nullable: false),
                        Total = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.IdFactura);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        IdProducto = c.Int(nullable: false, identity: true),
                        Producto = c.String(),
                    })
                .PrimaryKey(t => t.IdProducto);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Detalles", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.Detalles", "IdFactura", "dbo.Facturas");
            DropIndex("dbo.Detalles", new[] { "IdProducto" });
            DropIndex("dbo.Detalles", new[] { "IdFactura" });
            DropTable("dbo.Productos");
            DropTable("dbo.Facturas");
            DropTable("dbo.Detalles");
        }
    }
}
