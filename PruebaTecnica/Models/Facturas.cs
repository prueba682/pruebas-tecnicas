﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Models
{
    public class Facturas
    {
        [Key]
        public int IdFactura { get; set; }
        public int NumeroFactura { get; set; }

        [BindProperty,DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime Fecha { get; set; }
        public string TipodePago { get; set; }
        public string DocumentoCliente { get; set; }
        public string NombreCliente { get; set; }
        public double Subtotal { get; set; }
        public double Descuento { get; set; }
        public double IVA { get; set; }
        public double TotalDescuento { get; set; }
        public double TotalImpuesto { get; set; }
        public double Total { get; set; }

    }
}