﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Models
{
    public class RequestData
    {
        public Facturas Factura { get; set; }
        public List<Detalles> LstDetalles { get; set; }
    }
}