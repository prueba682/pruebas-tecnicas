﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Models
{
    public class Productos
    {
        [Key]
        public int IdProducto { get; set; }
        public string Producto { get; set; }
    }
}